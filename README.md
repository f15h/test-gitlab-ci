# Настройка необходимый приложений 

## Установка и настройка софта

### Настройка gitlab runner windows

#### Шаги

- Описаны тут: https://docs.gitlab.com/runner/install/windows.html

- Во время установки обязательно выбрать `shell`

- После установки я правил в папке с ним файл `runnner.toml` менял `pwsl` на `powershell`

### RClone 

Используется для выгрузки финального билда на Google Drive c удаленной машины

#### Шаги

- Скачать `https://rclone.org/downloads/`

- Скопировать файл в папку C:\rclone

- Заменить в файле .gitlab-ci.yml переменную `RCLONE_PATH`, к примеру `C:\rclone\rclone.exe`

- Открыть командную строку `cmd.exe`

- Перейти в папку `C:\rclone\` запустить в ней `rclone.exe config`

- Делаем по инструкции https://rclone.org/drive/#making-your-own-client-id

- После наcтройки надо будет перейти папкy: `C:\Users\bogor\AppData\Roaming\rclone` скопировать содержимое файла `rclone.conf`

- Добавить в Gitlab переменную окружения(Settings -> Ci|CD -> Variables): новую пекременную типа File с названием `RCLONE_CONFIG`. В значение скопировать значение файла `rclone.conf`


### Устновка Unreal Engine

И всего к нему

- Заменить в файле `.gitlab-ci.yml` переменную `UE5_ROOT`, на путь к установленному редактору `C:\UE_5_3`

### Установка python

#### Шаги

- Пропиши в переменную `PYTHON_PATH` путь до установленного python