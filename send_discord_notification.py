import requests
import sys

def send_discord_notification(webhook_url, message):
    data = {
        "content": message
    }
    response = requests.post(webhook_url, json=data)
    if response.status_code == 204:
        print("Message sent successfully.")
    else:
        print(f"Failed to send message. Status code: {response.status_code}, Response: {response.text}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python send_discord_notification.py <webhook_url> <message>")
        sys.exit(1)
    
    webhook_url = sys.argv[1]
    message = sys.argv[2]
    send_discord_notification(webhook_url, message)